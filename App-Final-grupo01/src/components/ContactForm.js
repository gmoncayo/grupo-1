import React, { useState, useEffect } from 'react';


const ContactForm = (props) => {
    const initialFieldValues = {
        fullName: '',
        Lastname:'',
        mobile: '',
        email: '',
        date: '',
        age:'',
        
        
        
    }

    var [values, setValues] = useState(initialFieldValues)


    useEffect(() => {
        if (props.currentId == '')
            setValues({ ...initialFieldValues })
        else
            setValues({
                ...props.contactObjects[props.currentId]
            })
    }, [props.currentId, props.contactObjects])

    const handleInputChange = e => {
        var { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        })
    }
    
        var dateactual= new Date();
		var separador;
		var fecha;
        var limite;
        
        var userToAdd = {};
        var errors=[]
        

    if(values.date.length>0){
        userToAdd.date = values.date;
        separador = "-"; // un espacio en blanco
        limite    = 1;
        fecha = parseInt(values.date.split(separador, limite),10);
        values.age = dateactual.getFullYear() - fecha; 
        //document.write(age);
        userToAdd.age = values.age;
    }else{
        errors.push('Fecha no puede estar vacío');
    }
    
    ///Final

    const handleFormSubmit = e => {
        e.preventDefault()
        props.addOrEdit(values);
    }

    return (
        <form autoComplete="off" onSubmit={handleFormSubmit}>
            
            <div className="form-group input-group">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-user"></i>
                    </div>
                </div>
                <input className="form-control"  type="text" name="fullName" placeholder="Nombre"  required autoComplete="off"
                    value={values.fullName}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group input-group">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-user"></i>
                    </div>
                </div>
                <input className="form-control" type="text" name="Lastname" placeholder="Apellido" required autoComplete="off"
                    value={values.Lastname}
                    onChange={handleInputChange}
                />
            </div>


            <div className="form-row">
                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i className="fas fa-mobile-alt"></i>
                        </div>
                    </div>

                    <input className="form-control" type="text" name="mobile" placeholder="Telefono" required autoComplete="off"
                        value={values.mobile}
                        onChange={handleInputChange}
                    />
                </div>
                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i className="fas fa-envelope"></i>
                        </div>
                    </div>
                    <input className="form-control" type="email" name="email" placeholder="Email"  required autoComplete="off"
                        value={values.email}
                        onChange={handleInputChange}
                    />
                </div>

                <div className="form-group input-group col-md-6">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i class="far fa-calendar"></i>
                        </div>
                    </div>
                    <input className="form-control" type="date" name="date" id="date" required min="1900-01-01" max="2019-12-31" autoComplete="off"
                        value={values.date}
                        onChange={handleInputChange}
                    />
                </div>

               
            </div>
          
            <div className="form-group">
                <input type="submit" value={props.currentId == "" ? "Save" : "Update"} className="btn btn-primary btn-block" />
            </div>

        </form>
    );
}

export default ContactForm;
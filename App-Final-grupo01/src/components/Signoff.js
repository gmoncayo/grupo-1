import React, {Component} from 'react';
import {auth} from '../firebase';

class Home extends Component {
    

    logout = () => {
        auth.signOut();
    }

    render(){
        return(
            <div>
                <button onClick={this.logout}>Cerrar Sesion</button>
            </div>
        )
    }
}

export default Home;
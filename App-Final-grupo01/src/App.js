/*import React  from 'react';
//import logo from './logo.svg';
import Contacts from './components/Contacts';

function App() {
  return (
    <div className="row">
      <div className="col-md-8 offset-md-2">
      <Contacts ></Contacts>
      </div>
    </div>
  );
}



export default App;*/


import React, {Component} from 'react';
import {auth} from './firebase';
import Login from './components/LoginRegister';
//import Signoff from './components/Signoff';
import './App1.css';
import Contacts from './components/Contacts';

class App extends Component {

  constructor(){
    super();
    this.state = {
      user: null
    }
  }

  componentDidMount(){
    this.authListener();
  }

  authListener(){
    auth.onAuthStateChanged((user) => {
      if(user){
        this.setState({user});
      }else{
        this.setState({user:null});
      }
    });
  }

  render(){
    return (
        <div>
          {this.state.user ? (<Contacts/>) : (<Login/>) }
        </div>
    );
  }
}

export default App;


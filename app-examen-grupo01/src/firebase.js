import * as firebase from "firebase";


var firebaseConfig = {
    apiKey: "AIzaSyB34k-MBUtxHfuFuhPWn7CGhAfQw4Utv1U",
    authDomain: "database-16986.firebaseapp.com",
    databaseURL: "https://database-16986.firebaseio.com",
    projectId: "database-16986",
    storageBucket: "database-16986.appspot.com",
    messagingSenderId: "735705415952",
    appId: "1:735705415952:web:885e0f6970e03e22c02595"
  };


var fireDb = firebase.initializeApp(firebaseConfig);

export default fireDb.database().ref();
export const auth = fireDb.auth();



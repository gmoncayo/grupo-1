import React from 'react'
import ContacForm from './ContactForm'
//Javier Sigcha literal 1

const MainCard = ({fullName, image, uid, like, dislike,}) => {
    return (
        <div className='main-card'>
            <img src={image} alt={fullName}/>
            <div>
                <h2>{fullName}</h2>
               

                <section>
                    <span className='like' onClick={()=>like(uid)}> 😏 </span>
                    <span className='dislike' onClick={()=>dislike(uid)}>  🙄 </span>
                </section>
            </div>
        </div>
    )
};

export default MainCard;
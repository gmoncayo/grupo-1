import React, {Component} from 'react';
import ProfileComponent from "./Reactions";
import CardsComponent from "./Header";
import firebaseDb from "../firebase";

//Javier Sigcha literal 1

class MainPage extends Component {
    state = {
        profile:{
            likes:[],
            dislikes:[],
            descripcion:'',
           
        },
        editar:false,
        users:[

        ],
    };

    
    

    getMyProfile=(u)=>{
        firebaseDb('likelike/users/'+u.uid)
            .on('value', snap=>{
                console.log(snap.val());
                this.setState({profile:snap.val()})
            })
    };
    getUsers=()=>{
        firebaseDb('likelike/users')
            .on('value', snap=>{
                this.setState({users:snap.val()});
                console.log(snap.val())
            });

    };


    saveUser=()=>{
        let profile= this.state.profile;
        let updates={};
        updates[`likelike/users/${profile.uid}`] = profile;
        firebaseDb.update(updates)
            .then(r=>{
            this.getMyProfile(profile);
        })
    };
    handleText=(e)=>{
        let profile=this.state.profile;
        let field = e.target.name;
        profile[field] = e.target.value;
        this.setState({profile});
        console.log(profile)
    };

    like=(user)=>{
        let updates={};
        let from = this.state.profile.uid;
        updates[`likelike/users/${user}/likes/${from}`] = true;
        firebaseDb.update(updates)
    };
    dislike=(user)=>{
        let updates={};
        let from = this.state.profile.uid;
        updates[`likelike/users/${user}/dislikes/${from}`] = true;
        firebaseDb.update(updates)
    };

    render() {
        let {profile, users, editar} = this.state;

        return (
            <div className="App">
                <div className='container'>
                    <ProfileComponent
                        {...profile}
                        
                        handleText={this.handleText}
                        
                        editar={editar}/>
                    <CardsComponent
                        like={this.like}
                        dislike={this.dislike}
                        users={users}/>
                </div>
            </div>
        );
    }
}

export default MainPage;
import React from 'react';
//Javier Sigcha literal 1

const ProfileComponent = ({username,  image, email, descripcion,  likes, dislikes, logOut,  handleText, saveUser}) => {
    return (
        <div className='profile-container'>
            <img src={image} alt={username}/>
            <div className='container'>
                <h2>{username}</h2>

                <section>
                    <p className='like'> 😏 {likes?Object.keys(likes).length:0} </p>
                    <p className='dislike'>  🙄 {dislikes?Object.keys(dislikes).length:0} </p>
                </section>

                <div className={"input-container"}>
                    <textarea placeholder={'Tu descripción'} name={'descripcion'} onChange={handleText} value={descripcion?descripcion:''}/>
                    
                </div>

                <span onClick={saveUser} >Editar</span>
                <span onClick={logOut}>Salir</span>
            </div>
        </div>
    )
};

export default ProfileComponent;

import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";

const data = [
  { id: 1, Nombre: "Johana", Apellido: "Criollo", Email: "jbcriollo@uce.edu.ec", Telefono: "0991385833", Nacimiento:"24/septiembre/1990", Edad: "29" },
  { id: 2, Nombre: "Katterine", Apellido: "Guadalupe", Email: "kjguadalupe@uce.edu.ec", Telefono: "0979059759", Nacimiento:"24/junio/1991", Edad: "29" },
  { id: 3, Nombre: "Grace", Apellido: "Pumisacho", Email: "gmpumisacho@uce.edu.ec", Telefono: "0983863252", Nacimiento:"28/noviembre/1995", Edad: "25" },
  { id: 4, Nombre: "Javier", Apellido: "Sigcha", Email: "gjsigcha@uce.edu.ec", Telefono: "0961333702", Nacimiento:"15/marzo/1994", Edad: "26" },

];

class App extends React.Component {
  state = {
    data: data,
    modalActualizar: false,
    modalInsertar: false,
    form: {
      id: "",
      Nombre: "",
      Apellido: "",
      Email: "",
      Telefono: "",
      Nacimiento: "",
      Edad: "",
    },
  };

  mostrarModalActualizar = (dato) => {
    this.setState({
      form: dato,
      modalActualizar: true,
    });
  };

  cerrarModalActualizar = () => {
    this.setState({ modalActualizar: false });
  };

  mostrarModalInsertar = () => {
    this.setState({
      modalInsertar: true,
    });
  };

  cerrarModalInsertar = () => {
    this.setState({ modalInsertar: false });
  };

  editar = (dato) => {
    var contador = 0;
    var arreglo = this.state.data;
    arreglo.map((registro) => {
      if (dato.id == registro.id) {
        arreglo[contador].Nombre = dato.Nombre;
        arreglo[contador].Apellido = dato.Apellido;
        //asignacion al arreglo
        arreglo[contador].Email = dato.Email;
        arreglo[contador].Telefono = dato.Telefono;
        arreglo[contador].Nacimiento = dato.Nacimiento;
        arreglo[contador].Edad = dato.Edad;
        
      }
      contador++;
    });
    this.setState({ data: arreglo, modalActualizar: false });
  };

  eliminar = (dato) => {
    var opcion = window.confirm("Estás Seguro que deseas Eliminar el elemento "+dato.id);
    if (opcion == true) {
      var contador = 0;
      var arreglo = this.state.data;
      arreglo.map((registro) => {
        if (dato.id == registro.id) {
          arreglo.splice(contador, 1);
        }
        contador++;
      });
      this.setState({ data: arreglo, modalActualizar: false });
    }
  };

  insertar= ()=>{
    var valorNuevo= {...this.state.form};
    valorNuevo.id=this.state.data.length+1;
    var lista= this.state.data;
    lista.push(valorNuevo);
    this.setState({ modalInsertar: false, data: lista });
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    
    return (
      <>
        <Container>
        <br />
          <Button color="success" onClick={()=>this.mostrarModalInsertar()}>Crear</Button>
          <br />
          <br />
          <Table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Nacimiento</th>
                <th>Edad</th>
                <th>Acción</th>
              </tr>
            </thead>

            <tbody>
              {this.state.data.map((dato) => (
                <tr key={dato.id}>
                  <td>{dato.id}</td>
                  <td>{dato.Nombre}</td>
                  <td>{dato.Apellido}</td>
                  <td>{dato.Email}</td>
                  <td>{dato.Telefono}</td>
                  <td>{dato.Nacimiento}</td>
                  <td>{dato.Edad}</td>
                  
                  <td>
                    <Button
                      color="primary"
                      onClick={() => this.mostrarModalActualizar(dato)}
                    >
                      Editar
                    </Button>{" "}
                    <Button color="danger" onClick={()=> this.eliminar(dato)}>Eliminar</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalActualizar}>
          <ModalHeader>
           <div><h3>Editar Registro</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
               Id:
              </label>
            
              <input
                className="form-control"
                readOnly
                type="text"
                value={this.state.form.id}
                
              />
            </FormGroup>
            
            <FormGroup>
              <label>
                Nombre: 
              </label>
              <input
                className="form-control"
                name="Nombre"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.Nombre}
                required class="form-control"
              />
            </FormGroup>
            
            <FormGroup>
              <label>
              Apellido: 
              </label>
              <input
                className="form-control"
                name="Apellido"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.Apellido}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Email: 
              </label>
              <input
                className="form-control"
                name="Email"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.Email}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Telefono: 
              </label>
              <input
                className="form-control"
                name="Telefono"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.Telefono}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Nacimiento: 
              </label>
              <input
                className="form-control"
                name="Nacimiento"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.Nacimiento}
                
              />
            </FormGroup>

            <FormGroup>
              <label>
              Edad: 
              </label>
              <input
                className="form-control"
                name="Edad"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.Edad}
              />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.editar(this.state.form)}
            >
              Editar
            </Button>
            <Button
              color="danger"
              onClick={() => this.cerrarModalActualizar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>



        <Modal isOpen={this.state.modalInsertar}>
          <ModalHeader>
           <div><h3>Insertar Nuevo Estudiante</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id: 
              </label>
              
              <input
                className="form-control"
                readOnly
                type="text"
                value={this.state.data.length+1}
              />
            </FormGroup>
            
            <FormGroup>
              <label>
                Nombre: 
              </label>
              <input
                className="form-control"
                name="Nombre"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>
            
            <FormGroup>
              <label>
              Apellido: 
              </label>
              <input
                className="form-control"
                name="Apellido"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Email: 
              </label>
              <input
                className="form-control"
                name="Email"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Telefono: 
              </label>
              <input
                className="form-control"
                name="Telefono"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Nacimiento: 
              </label>
              <input
                className="form-control"
                name="Nacimiento"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
              Edad: 
              </label>
              <input
                className="form-control"
                name="Edad"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.insertar()}
            >
              Insertar
            </Button>
            <Button
              className="btn btn-danger"
              onClick={() => this.cerrarModalInsertar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
export default App;

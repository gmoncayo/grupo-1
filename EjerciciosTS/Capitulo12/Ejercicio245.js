"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function MostrarEjer245() {
    var Ship = /** @class */ (function () {
        function Ship(name, type, color) {
            this.name = name;
            this.type = type;
            this.color = color;
        }
        Ship.prototype.shipName = function () {
            return 'I am ' + this.name;
        };
        Ship.prototype.shipType = function () {
            return 'I am type: ' + this.type;
        };
        Ship.prototype.shipColor = function () {
            return 'My color is ' + this.color;
        };
        return Ship;
    }());
    var SpaceShip = /** @class */ (function (_super) {
        __extends(SpaceShip, _super);
        function SpaceShip(type, name, color) {
            return _super.call(this, type, name, color) || this;
        }
        SpaceShip.prototype.spaceShipName = function () {
            return _super.prototype.shipName.call(this);
        };
        SpaceShip.prototype.spaceShipType = function () {
            return _super.prototype.shipType.call(this);
        };
        SpaceShip.prototype.spaceShipColor = function () {
            return _super.prototype.shipColor.call(this);
        };
        return SpaceShip;
    }(Ship));
    var planetExpress = new SpaceShip('Planet Express Ship', 'Delivery Ship', 'Green');
    console.log(planetExpress.spaceShipName()); //returns I am Planet Express Ship
    console.log(planetExpress.shipType()); //return I am type: 'Delivery Ship
    console.log(planetExpress.spaceShipColor()); // returns My color is Green
}

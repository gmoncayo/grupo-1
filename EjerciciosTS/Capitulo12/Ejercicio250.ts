function MostrarEjer250() {
function showSpread(one:Number, two:Number, three:Number, four:Number){
    console.log(one);
    console.log(two);
    console.log(three);
    console.log(four);
}
var myArray: number[] = [1,2,3,4];
//showSpread(myArray[0], myArray[1], myArray[2], myArray[3]) //without using spread
//showSpread(...myArray); //using the spread syntax
var dayInfo: number[]= [1975, 7, 19];
var dateObj: Date  = new Date(dayInfo[0], dayInfo[1], dayInfo[2]);
console.log(dateObj); //returns Tue Aug 19 1975 00:00:00 GMT-0400 (EDT)
var numArray2: number[]  = [ 2, 3, 4, 5, 6, 7]
var numArray: number[] = [1, ...numArray2, 8, 9, 10];
console.log(numArray); //returns 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
var part1: number[]= [1,2,3];
var part2: number[]= [4,5,6];
var part3: number[]= [...part1, ...part2];
console.log(part3); //returns 1,2,3,4,5,6
}
"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
function MostrarEjer264() {
    function countdown(stringLiteralArray) {
        var values = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            values[_i - 1] = arguments[_i];
        }
        console.log(stringLiteralArray); //returns full array
        console.log(stringLiteralArray[1]); //returns Mississippi
        console.log(values); //returns array of values
        console.log(values[0]); //returns 1
        console.log(values[1]); //returns 2
        var fullSentance = values[0] + stringLiteralArray[1] + values[1] + stringLiteralArray[2];
        return fullSentance;
    }
    var one = 1;
    var two = 2;
    var results = countdown(__makeTemplateObject(["", " Mississippi ", " Mississippi"], ["", " Mississippi ", " Mississippi"]), one, two);
    console.log(results); //returns 1 Mississippi 2 Mississippi
}

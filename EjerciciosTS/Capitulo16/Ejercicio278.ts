function MostrarEjer278() {
  function trapMessage() {
    return "It's a Trap!!!";
  }
  var handler = {
    apply: function (target: any, thisArg: any) {
      return target.apply(thisArg);
    },
  };
  var proxy = new Proxy(trapMessage, handler);
  console.log(proxy()); //returns It's a Trap!!
}

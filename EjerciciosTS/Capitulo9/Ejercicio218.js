"use strict";
function MostrarEjer218() {
    // Definición
    var robot1 = { type: "Autobot", vehicle: "Truck" };
    var robot2 = { type: "Autobot", vehicle: "Truck" };
    var robot3 = { type: "Autobot", vehicle: "Truck", likes: "Music" };
    // Listado 9-15. Prueba si los objetos son inmutables
    var optimusPrime = Object.freeze(robot1);
    console.log(Object.isFrozen(optimusPrime)); //retorna true
    var hotRod = Object.seal(robot2);
    console.log(Object.isSealed(hotRod)); //retorna true
    var jazz = Object.preventExtensions(robot3);
    console.log(Object.isExtensible(jazz)); //retorna false
}

function MostrarEjer209() {
// Listado 9-4. Usar variables para acceder a las propiedades de un objeto
var ship:any = {};
var type = 'spaceShip';
ship[type] = 'X-Wing';
console.log(ship[type]); //retorna X-Wing
console.log(ship.spaceShip);  //retorna X-Wing
console.log(ship.type);  //retorna undefined
}
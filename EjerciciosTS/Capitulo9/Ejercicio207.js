"use strict";
function MostrarEjer207() {
    // Listado 9-1. Los objetos se pueden crear utilizando el Constructor de objetos o creando un objeto literal
    var R2D2 = new Object();
    R2D2.class = 'Astromech Droid';
    var R2D2 = {
        class: 'Astromech Droid',
        manufacturer: 'Industrial Automaton'
    };
    console.log(R2D2);
}

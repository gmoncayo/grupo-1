"use strict";
function MostrarEjer188() {
    //Listado 7-13. El método splice () le permite agregar o quitar elementos de la matriz actual
    var marxBros = ['Groucho', 'Harpo', 'Chico'];
    marxBros.splice(2, 0, 'Zeppo');
    console.log(marxBros); //retorna ['Groucho', 'Harpo', 'Chico', 'Zeppo']
    marxBros.splice(1, 3);
    console.log(marxBros); //retorna ['Groucho']
}

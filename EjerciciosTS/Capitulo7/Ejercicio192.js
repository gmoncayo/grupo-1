"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
function MostrarEjer192() {
    var _a;
    // Listado 7-18. La diferencia entre Find y FindIndex
    function myValues() {
        var values = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            values[_i] = arguments[_i];
        }
        return values;
    }
    console.log(myValues(1, 2, 3)); // returns [1, 2, 3]
    console.log(myValues(1, 2, 3, 4, 5, 6)); // returns [1, 2, 3, 4, 5, 6]
    var firstThree = ['One', 'Two', 'Three'];
    var myArray = __spreadArrays(firstThree, [4, 5, 6]);
    var otherShips, a, b;
    console.log(myArray); // returns  ["One", "Two", "Three", 4, 5, 6]
    _a = ['Tardis', 'X-Wing', 'B-Wing', 'Enterprise', 'Moya'], a = _a[0], b = _a[1], otherShips = _a.slice(2);
    console.log(otherShips); // returns ["B-Wing", "Enterprise", "Moya"]
}

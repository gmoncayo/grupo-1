function MostrarEjer189() {
// Listado 7-15. El método pop () eliminará el último elemento de la matriz
var marxBros = ['Groucho', 'Harpo', 'Chico']
marxBros.pop();
console.log(marxBros); //retorna ['Groucho',  'Harpo']
}
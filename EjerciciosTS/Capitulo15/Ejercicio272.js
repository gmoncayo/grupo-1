"use strict";
function MostrarEjer272() {
    var _a;
    var PrimitiveObj = (_a = {},
        _a[Symbol.toPrimitive] = function (hint) {
            if (hint == 'number') {
                return 100;
            }
            else if (hint == 'string') {
                return 'this is a string';
            }
            else {
                return 'this is the default';
            }
        },
        _a);
    console.log(PrimitiveObj);
}

function MostrarEjer178(argument) {
//Quiero saber cuál es el desplazamiento entre la hora UTC y mi hora local
var currentDate = new Date();
var offSet = currentDate.getTimezoneOffset() / 60;
console.log(offSet);
//verifique si estamos en horario de verano
var today = new Date();
function isDST() {
    var jan = new Date(today.getFullYear(), 0, 1);
    var jul = new Date(today.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}
if (isDST() != today.getTimezoneOffset()) {
    console.log('On DST');
}
else {
    console.log('Not On DST');
}
// body...
}
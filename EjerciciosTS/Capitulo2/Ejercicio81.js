"use strict";
function MostrarEjer81() {
    var house = { bedrooms: 4, floors: 3 };
    if ('bedrooms' in house) {
        console.log('house has a "bedrooms" property');
    }
    if ('floors' in house) {
        console.log('house has a "floors" property');
    }
    if ('style' in house) {
        console.log('house has a "style" property');
    }
    else {
        console.log('house does not have a "style" property');
    }
    var bedroomPropertyName = 'bedrooms';
    if (bedroomPropertyName in house) {
        console.log('house has a ' + bedroomPropertyName + ' property');
    }
    var qualifiers = ['First', 'Second', 'Third', 'DNF'];
    if (0 in qualifiers) {
        console.log('qualifiers has a "0" item');
    }
    if (3 in qualifiers) {
        console.log('qualifiers has a "3" item');
    }
    if (8 in qualifiers) {
        console.log('qualifiers has an "8" item');
    }
    else {
        console.log('qualifiers does not have an "8" item');
    }
    var myDate = new Date();
    if ('setDay' in myDate) {
        console.log('myDate has a "setDay" method');
    }
    // body...
}

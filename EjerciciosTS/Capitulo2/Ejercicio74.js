"use strict";
function MostrarEjer74() {
    //Listado 2-18. Uso del operador de negación lógica (!) Para alternar un valor booleano
    console.log(!true); // False
    console.log(!false); // True
    console.log(!0); // True
    console.log(!1); // False
    console.log(!!1); // Se puede usar varias veces el ! para cambiar el resulatdos y sea true
}

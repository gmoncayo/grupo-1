"use strict";
function MostrarEjer122() {
    if ('Mike'.localeCompare('John') === -1) {
        console.log('Mike viene antes que John');
    }
    else if ('Mike'.localeCompare('John') === 1) {
        console.log('John viene antes que Mike');
    }
    else {
        console.log('John and Mike are both in the same position');
    }
    if ('Jan'.localeCompare('John') === -1) {
        console.log('Jan viene antes que John');
    }
    else if ('Jan'.localeCompare('John') === 1) {
        console.log('John viene antes que Jan');
    }
    else {
        console.log('John and Jan are both in the same position');
    }
    if ('a'.localeCompare('á') === -1) {
        console.log('a viene antes que á');
    }
    else if ('a'.localeCompare('á') === 1) {
        console.log('á viene antes que a');
    }
    else {
        console.log('a and á are the same kind of letter');
    }
    if ('Michelle'.localeCompare('Michéllé') === -1) {
        console.log('Michelle viene antes que Michéllé');
    }
    else if ('Michelle'.localeCompare('Michéllé') === 1) {
        console.log('Michéllé viene antes que Michelle');
    }
    else {
        console.log('Michéllé y Michelle Michelle  están en la misma posición');
    }
}

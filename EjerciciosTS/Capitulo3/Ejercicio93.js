"use strict";
function MostrarEjer93() {
    console.log('this\nis\na\nmultiline\nstring'); // n  en diferentes lineas
    console.log('\tthis\tstring\thas\ttabs\tinstead\tof\tspaces'); //  t  en una misma lineas
    console.log('this string uses \'single quotes\', and includes escaped \'single quotes\' inside it'); // s en una linea separada con comillas
}

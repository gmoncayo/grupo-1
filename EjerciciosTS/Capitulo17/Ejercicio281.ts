
// Listado 17-1. Crear una clase ES5 y una clase ES6

//ECMAScript 5 class
var human = (function human(this: any, name: any){this.name = name;})
human.prototype.sayGoodNight = function(){ return 'Say Goodnight ' + this.name;}
var george:string = ('Gracie');
console.log('Say Goodnight ');

//ECMAScript 6 class
class Greeting{
    name: string;constructor(name: string){ this.name = name;}
   sayHello(){
       return 'Hellooo ' + this.name;
             }
}
var yakko = new Greeting('Nurse!');
console.log(yakko.sayHello());

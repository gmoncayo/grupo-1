function MostrarEjer281() {
// Listado 17-1. Crear una clase ES5 y una clase ES6
//ECMAScript 5 class
var human = (function human(name) { this.name = name; });
human.prototype.sayGoodNight = function () { return 'Say Goodnight ' + this.name; };
var george = ('Gracie');
console.log('Say Goodnight ');
//ECMAScript 6 class
var Greeting = /** @class */ (function () {
    function Greeting(name) {
        this.name = name;
    }
    Greeting.prototype.sayHello = function () {
        return 'Hellooo ' + this.name;
    };
    return Greeting;
}());
var yakko = new Greeting('Nurse!');
console.log(yakko.sayHello());
}
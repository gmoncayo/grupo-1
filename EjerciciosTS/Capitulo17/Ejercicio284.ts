
// Listado 17-3. Usando Getters / Setters en una clase ES6
class Cookies{
    _typeOfCookie: any;
    constructor(){
        this._typeOfCookie;
    }
    set cookieType(typeOfCookie){
        this._typeOfCookie = typeOfCookie;
    }
    get cookieType(){
        return this._typeOfCookie;     
    }
}
var myCookie = new Cookies();
myCookie.cookieType = "Chocolate Chip";
console.log(myCookie.cookieType); //retorna Chocolate Chip;
console.log(myCookie._typeOfCookie);


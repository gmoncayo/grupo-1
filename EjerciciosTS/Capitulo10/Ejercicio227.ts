    var numberSet = new Set();
	numberSet.add(1);
 	numberSet.add(2);
 	numberSet.add(3);
 	numberSet.add('things');
 	console.log(numberSet.keys()); //devuelve SetIterator {1, 2, 3, "things"}
 	console.log(numberSet.values()); //devuelve SetIterator {1, 2, 3, "things"}
 	var elements = numberSet.values();
 	console.log(elements.next().value); //devuelve 1
 	console.log(elements.next().value); //devuelve 2
	// body...

/*
¿Cuál es la diferencia entre las claves y los valores?
¿Métodos?
Problema
Debe saber cuándo usar el método de claves sobre el método de valores.
Solución
Ambos métodos devuelven un objeto iterador que contiene valores para cada elemento. El método de claves es un alias
para el método de valores.

Cómo funciona
Los métodos de valores y claves devuelven un objeto iterador. Si desea ver manualmente el siguiente elemento,
Puedes usar el siguiente método.*/
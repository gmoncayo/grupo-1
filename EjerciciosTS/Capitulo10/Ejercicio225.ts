	var numberSet = new Set();
	numberSet.add(1);
 	numberSet.add(2);
 	numberSet.add(3);
 	numberSet.add(3); //no se agrega
 	console.log(numberSet.entries()); //devuelve SetIterator {[1, 1], [2, 2], [3, 3]}
	var numArray = new Array();
 	numArray.push(1);
 	numArray.push(2);
 	numArray.push(3);
 	numArray.push(3);
 	console.log(numArray); //devuelve [1, 2, 3, 3]
	// body...


/*¿Cuál es la diferencia entre un conjunto y una matriz?
Problema
¿En qué situación usarías un objeto set sobre una matriz?
Solución
Hay algunas similitudes entre los dos objetos. Ambos pueden conservar datos de diferentes tipos. Qué
separa un conjunto es que todos los valores deben ser únicos.

Cómo funciona
Algunas de las propiedades y métodos asociados con un conjunto son muy similares a lo que encontraría con un
formación. Por ejemplo, la propiedad de tamaño es como usar la propiedad de longitud en una matriz. Donde las matrices pueden tener
múltiples elementos con el mismo valor, los conjuntos están hechos para contener valores únicos para cada elemento*/
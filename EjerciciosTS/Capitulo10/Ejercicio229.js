function MostrarEjer229() {
var numberSet = new Set();
numberSet.add(1);
numberSet.add(2);
numberSet.add(3);
numberSet.forEach(function (value) {
    console.log(value); //devuelve 1,2,3
});
}
// body...
/*¿Cómo funciona un método forEach con un objeto establecido?
Problema
Debe saber si hay una diferencia usando un método forEach en un conjunto frente a una matriz.
Solución
El método forEach funciona igual cuando se usa un conjunto que cuando se usa una matriz. El método se llama para cada
valor en el conjunto.

Cómo funciona
Para ser coherente con las versiones de mapa y matriz, se llamará al método forEach una vez para cada
elemento en un conjunto. Cuando se llama a la función, se envían tres argumentos. Los dos primeros serán el valor de
el elemento. El tercero habría sido el objeto establecido que se está atravesando.*/ 

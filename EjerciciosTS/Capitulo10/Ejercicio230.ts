	var bandSet = new Set();
 	bandSet.add('Grace');
 	bandSet.add('Martin');
 	bandSet.add('Johana');
	bandSet.add('Jim');
 	bandSet.add('Paul');
 	bandSet.add('Javi');
 	bandSet.add('Andy');
 	bandSet.add('Katty');
 	var entry = bandSet.entries();
 	console.log(entry.next().value); // devuelve ["Grace", "Grace"]
 	console.log(entry.next()); //devuelve Object {value: Array[2], done: falsp}
 	while(entry.next().done == false){ //siempre y cuando el elemento actual devuelva falso, continúe
 		console.log(entry.next().value)
 	}
	// body...


/*¿Puede un conjunto generar objetos iteradores?
Problema
Desea personalizar cómo itera a través de un conjunto.
Solución
El método de entradas devolverá una matriz de cada elemento en el orden de inserción.

Cómo funciona
Al utilizar el método de entradas, puede crear un iterador personalizado para el conjunto. (Para más información sobre
iteradores, consulte el Capítulo 13.) El objeto iterador contiene dos propiedades, hecho y valor. Los conjuntos no tienen
teclas de la misma manera que lo hace un mapa. Debido a esto, devuelve el mismo valor que la clave y la clave
valor. Esto mantiene la API similar al objeto Map.
La propiedad done devuelve un valor booleano que le permite saber si ha alcanzado el último elemento en secuencia.
El valor es falso hasta que se alcanza el último elemento.
La propiedad value devuelve una matriz. Como los objetos establecidos no tienen una clave, el valor de ambos elementos en
La matriz es la misma. Esto lo mantiene consistente con el objeto Map.*/
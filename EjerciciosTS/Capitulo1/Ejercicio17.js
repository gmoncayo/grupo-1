"use strict";
function MostrarEjer17() {
    var myNumber = 8;
    // Determine si myNumber es equivalente al número 10
    if (myNumber === 10) {
        console.log(" Mi numero es igual a 10");
    }
    else {
        console.log("Mi numero no es igual a 10");
    }
    // Determine si myNumber es menor que el número 10
    if (myNumber < 10) {
        console.log("Mi numero es menor a 10");
    }
    else {
        console.log("Mi numero  es igual o mayor a 10");
    }
    // Determine si myNumber es menor o igual que el número 10
    if (myNumber <= 10) {
        console.log("Mi numero es menor o igual a 10");
    }
    else {
        console.log("Mi numero es mayor a 10");
    }
    // Determine si myNumber es mayor que el número 10
    if (myNumber > 10) {
        console.log("Mi numero es mayor a 10");
    }
    else {
        console.log("Mi numero es menor o igual a 10");
    }
    // Determine si myNumber es mayor o igual que el número 10
    if (myNumber >= 10) {
        console.log("Mi numero es mayor o igual a 10");
    }
    else {
        console.log("Mi numero es menor a 10");
    }
}

"use strict";
function MostrarEjer19() {
    var a;
    var b = null;
    if (typeof a === 'undefined') {
        console.log("Variable a es indefinida");
    }
    if (a === null) {
        console.log("Variable a es nula");
    }
    if (typeof b === 'undefined') {
        console.log("Variable b es indefinida");
    }
    if (b === null) {
        console.log("Variable b es nula");
    }
    // body...
}
//Diferencia entre nulo e indefinido

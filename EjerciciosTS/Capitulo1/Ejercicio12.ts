//1_12 Determinar si una variable ha sido declarada usando typeof
// La declaración para la variable a se comenta,
// por lo tanto, a no se ha declarado.
function MostrarEjer12() {
  var a,
    b = 4;
  if (typeof a === "undefined") {
    //Operador de igualdad estricta (`===`)
    console.log("Variable 'a' is undefined");
  }
  if (typeof b === "undefined") {
    console.log("Variable 'b' is undefined");
  }
}

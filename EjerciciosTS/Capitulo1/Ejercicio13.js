"use strict";
function MostrarEjer13() {
    // Una funcion con una declaración
    // devuelve indefinido
    function function1() {
        return;
    }
    //Una funcion sin declaración de retorno
    // devuelve indefinido
    function function2() { }
    function function3() {
        return 2 + 2;
    }
    function function4() {
        return true;
    }
    function function5() {
        return {};
    }
    var fn1 = function1();
    console.log("Function1 returns: " + fn1); // indefinido
    var fn2 = function2();
    console.log("Function2 returns: " + fn2); // indefinido
    var fn3 = function3();
    console.log("Function3 returns: " + fn3); // 4
    var fn4 = function4();
    console.log("Function4 returns: " + fn4); // true
    var fn5 = function5();
    console.log("Function5 returns: " + fn5); // Object{}
    // Test returna el valor de la function1
    if (function1() === undefined) {
        console.log("Function 1 Devuelve indefinido.");
    }
    else {
        console.log("Function 1 devuelve un valor distinto de indefinido.");
    }
    // Test returna el valor de la function2
    if (function2() === undefined) {
        console.log("Function 2 Devuelve indefinido.");
    }
    else {
        console.log("Function 2 devuelve un valor distinto de indefinido.");
    }
    // Test returna el valor de la function3
    if (function3() === undefined) {
        console.log("Function 3 Devuelve indefinido.");
    }
    else {
        console.log("Function 3 devuelve un valor distinto de indefinido.");
    }
    // Test returna el valor de la function4
    if (function4() === undefined) {
        console.log("Function 4 Devuelve indefinido.");
    }
    else {
        console.log("Function 4 devuelve un valor distinto de indefinido.");
    }
    // Test returna el valor de la function5
    if (function5() === undefined) {
        console.log("Function 5 Devuelve indefinido.");
    }
    else {
        console.log("Function 5 devuelve un valor distinto de indefinido.");
    }
}

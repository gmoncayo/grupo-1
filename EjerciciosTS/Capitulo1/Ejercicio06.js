"use strict";
function MostrarEjer06() {
    //Primer Ejemplo
    let i = 0;
    for (let i = 0; i < 4; ++i) {
        console.log(i); // Se imprime 0, 1, 2, y3
    }
    console.log(i);
    // Intercambia dos variables:
    var x = 1, y = 2;
    {
        var temp = x;
        x = y;
        y = temp;
    }
    console.log(x, y, typeof temp); // imprime '2, 1, undefined'
    // segundo ejemplo
    let a = 4; // Se declara la variable "a" y se le asigna el valor de 4
    let b = 2; // Se declara la variable "b" y se le asigna el valor de 2
    console.log(b); // Imprime 2
    if (a === 4) {
        let a = 1;
        console.log(a);
    }
    console.log(b);
    console.log(a);
    // body...
}

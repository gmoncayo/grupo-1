"use strict";
function MostrarEjer11() {
    var a; // La variable "a" ha sido declarada pero no se le ha asignado una definición.
    var b = 5; // La variable "b" ha sido declarada y se le ha asignado el número 5 como su definición.
    if (a === undefined) { //compara la variable a 
        console.log("Variable 'a' es indefinida");
    }
    else {
        console.log("Variable 'a' es definita");
    }
    if (b === undefined) { //compara la variable b
        console.log("Variable 'b' es indefinida");
    }
    else {
        console.log("Variable 'b' es definida");
    }
}

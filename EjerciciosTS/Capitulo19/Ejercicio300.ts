function MostrarEjer300() {
  let rngError: string = "The value is out of range";
  console.log(rngError); //returns The value is out of range
  let refError: string = "This reference is not valid";
  console.log(refError); //returns This reference is not valid
  class myCustomError extends Error {
    constructor(message: string | undefined) {
      super(message);
    }
  }
  let myCustomErrorInstance: string = "This is a Custom Error";
  console.log(myCustomErrorInstance); //returns This is a Custom Error
  console.log(myCustomErrorInstance); //returns stack trace
  try {
    throw new myCustomError("There has been a mistake");
  } catch (e) {
    console.log(e.message); //returns There has been a mistake
    console.log(e.stack); //returns stack trace
  }
}

"use strict";
function MostrarEjer147() {
    var numSequence = 123456789;
    var reversedNumbers = Number(numSequence.toString().split('').reverse().join(''));
    console.log(reversedNumbers); // devuelve 4321
    //sin convertir a una cadena
    var a = 123456789, b = 0;
    while (a > 0) {
        b = b * 10;
        b = b + parseInt((a % 10).toString());
        a = parseInt((a / 10).toString());
    }
    console.log("Numero invertido: " + b);
}

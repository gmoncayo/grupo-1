function MostrarEjer143 () {

	function getRandomBetweenMinAndMax(min:any, max:any) {
		return Math.floor(Math.random() * (max - min) + min);
	}
	function getRandomArbitrary(min:any, max:any) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
	
	console.log('generador de numeros aleatorios');
	console.log(getRandomBetweenMinAndMax(0,5));
	console.log(getRandomArbitrary(0,5));
	// body...
}
"use strict";
function MostrarEjer139() {
    var numberObject = new Number('things');
    function checkIsNaN(value) {
        if (typeof value !== 'number') {
            return 'no es un numero';
        }
        else {
            return 'es un numero';
        }
    }
    console.log('comprobando si valor = things');
    console.log(checkIsNaN('1234')); //returns is not a number
    console.log(Number.isNaN('things')); // returns true
    console.log(Number.isNaN(numberObject.valueOf())); //returns true
    console.log(Number.isNaN(4)); //returns false
    console.log(Number.isNaN(NaN)); //returns true
    // body...
}

"use strict";
function MostrarEjer149() {
    function swapNumbers(numb1, numb2) {
        console.log("orden inicial = " + numb1 + "," + numb2);
        numb2 = numb2 - numb1; //es ahora -150
        numb1 = numb1 + numb2; //es ahora 50
        numb2 = numb1 - numb2; //es ahora 200
        console.log("orden final = " + numb1 + "," + numb2);
    }
    swapNumbers(200, 50);
}

"use strict";
function MostrarEjer238() {
    var mapObj = new Map();
    mapObj.set('1st value', '1st key');
    mapObj.set('2nd value', '2nd key');
    mapObj.set('3rd value', '3rd key');
    mapObj.forEach(function (value, key) {
        console.log('mapObj[' + key + '] = ' + value); //devuelve mapObj[N value] = N key
    });
}

"use strict";
function MostrarEjer235() {
    // Listado 11-5. El método Keys devolverá un objeto MapIterator que le permite acceder a las claves de cada elemento
    var mapObj = new Map();
    mapObj.set('1st value', '1st key');
    mapObj.set('2nd value', '2nd key');
    mapObj.set('3rd value', '3rd key');
    console.log(mapObj.keys()); //retorna  MapIterator object
    var mapIterator = mapObj.keys();
    console.log(mapIterator.next().value); // retorna 1st value
    console.log(mapIterator.next().value); // retorna 2nd value
    // body...
}

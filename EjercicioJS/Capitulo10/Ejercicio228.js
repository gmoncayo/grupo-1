function MostrarEjer228() {
	var weakSet = new WeakSet();
	var obj1 = {};
	weakSet.add(obj1);
	console.log(weakSet.has(obj1)); //Devuelve verdadera al igual que un conjunto
	// body...
}

/*¿Qué es un conjunto débil?
Problema
Necesita saber la diferencia entre un conjunto y un WeakSet.
Solución
WeakSets solo se aferra a los objetos, donde un conjunto puede aferrarse a elementos de cualquier tipo. Referencias a objetos en el
la recolección es basura recolectada si no hay referencias a ellos.

Cómo funciona
Los métodos de un conjunto débil son los mismos que para un objeto conjunto. La principal diferencia entre los dos es que solo
Los objetos se pueden guardar como elementos. Los WeakSets tampoco son enumerables y se recolectan basura si hay
sin referencias a objetos en el conjunto débil.*/
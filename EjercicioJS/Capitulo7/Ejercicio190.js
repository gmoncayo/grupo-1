function MostrarEjer190(argument) {
// Listado 7-17. Ejemplos de indexOf () y lastIndexOf ()
var albumNames = ['Hack', 'Violator', 'Designation', 'Wild', 'Surrender', 'Hack'];
console.log(albumNames.indexOf('Hack')); //retorna 0
console.log(albumNames.lastIndexOf('Hack')); //retorna 5
}
function MostrarEjer208() {
// Listado 9-2. Acceso a las propiedades de un objeto mediante notación de puntos y sintaxis de corchetes
var R2D2  = {};
R2D2['class'] = 'Astromech Droid'
console.log(R2D2['class']);  //retorna Astromech Droid
R2D2.manufacturer = 'Industrial Automaton';
console.log(R2D2.manufacturer);  //retorna Industrial Automaton
}
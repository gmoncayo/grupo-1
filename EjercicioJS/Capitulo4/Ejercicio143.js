function MostrarEjer143 () {

	function getRandomBetweenMinAndMax(min, max) {
		return Math.floor(Math.random() * (max - min) + min);
	}
	function getRandomArbitrary(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
	
	console.log('generador de numeros aleatorios');
	console.log(getRandomBetweenMinAndMax(0,5));
	console.log(getRandomArbitrary(0,5));
	// body...
}

/* Crear un generador de números aleatorios
Problema
Desea generar un número aleatorio entre dos valores.
Solución
Utilice el método aleatorio en el objeto matemático y redondee los resultados.

Cómo funciona
Por sí solo, el método aleatorio en el objeto Math devuelve un número pseudoaleatorio de coma flotante en el
rango de 0 o 1. El resultado sería una fracción de un número que luego necesitaría redondearse usando el
método de piso. Puede establecer un rango mínimo y máximo creando una función que establezca estos valores.
En su función, pasa los valores máximos y mínimos. Para que JavaScript entienda
estos valores, necesita hacer un poco de matemática. Dentro de los paréntesis, restas el número máximo del
número mínimo Por sí solo eso debería darle el valor del número mínimo. Esto es lo que se llama el
número inclusivo Fuera de los paréntesis, agrega el número mínimo y eso le dará JavaScript
El número máximo. Este es el número exclusivo. Este número no será parte de los resultados.
Esto funcionará bien, pero terminará sin tener el número alto como parte del resultado, ya que es el
número excluido Para cambiar esto sobre uno, agrega uno a la ecuación entre paréntesis. Esta voluntad
aumente el valor máximo para que el número que desea usar se incluya en los resultados.*/
function MostrarEjer147() {

	var numSequence = 123456789;
	var reversedNumbers = Number(numSequence.toString().split('').reverse().join(''));
	console.log(reversedNumbers) // devuelve 4321

	//sin convertir a una cadena
	var a = 123456789, b=0;
	while(a > 0){
		b = b * 10;
		b = b + parseInt(a%10);
		a = parseInt(a/10);
	}

	console.log("Número invertido: " + b);
	// body...
}

/*Devolver un número en reversa
Problema
Se le da un número y se le pide que devuelva ese número al revés. Por ejemplo, si le dan 123
El resultado debe ser 321.
Solución
Con JavaScript, hay muchas formas de hacerlo. La forma más rápida es convertir todo en una cadena.
y use el método inverso y luego convierta todo nuevamente en un número. Esta es también una forma rápida de ver si un
algo es un palíndromo si se le pregunta en una entrevista, ya que los resultados deberían tener el valor.
En el segundo ejemplo, puede usar un ciclo while y eliminar el último número de la primera variable y
agréguelo al segundo, comprobando cada vez si el valor del primer número aún no es 0.

Cómo funciona
El primer ejemplo es muy simple. Como puedes hacer conversión de tipo muy rápidamente con JavaScript, puedes
encadenar algunos métodos juntos. Todo esto debe tener lugar dentro de Number () o parseInt ()
método, por lo que los resultados se pueden convertir de nuevo al tipo de datos de número; de lo contrario, terminas con una cadena.
Dentro de su método elegido, convierta el número entero en una cadena usando el método toString (). Cadena
el método de división (‘’) para eso. Asegúrese de tener comillas vacías dentro de los paréntesis, porque el
la división se basa en el espacio entre los personajes. El resultado de esto es ahora una serie de cadenas. Añade el
Método reverse () para invertir el orden de la matriz. Luego, finalmente, vuelva a unir los elementos en una cadena usando
el método join (‘’). Para asegurarse de recuperar un tipo de datos numérico, puede poner los resultados en el navegador
consola usando el método console.log () y use typeof para devolver el tipo de datos.
La segunda forma no usa conversión de tipo. De esta manera, ambas variables se inicializan. El segundo
La variable (en nuestro caso b) tiene un valor de 0. Todo el trabajo se realiza dentro de un ciclo while. Cada vez que pasas
En el bucle, verifica si el valor de a es mayor que 0. Si es así, primero toma b y multiplícalo por 10. El siguiente
line tomará el último número de a y lo agregará a b, usando una combinación del método parseInt y el
operador de módulo. Este operador tomará el número dividido por 10 y le dará el resto. En este caso,
Este es el último número.
Puede notar que todas las matemáticas se basan en 10 en este ejemplo. Estamos usando 10 de manera similar a como se usa en
El método parseInt () como la raíz. Cada vez que multiplicamos b por 10, nos aseguramos cuando se suman los números
juntos obtenemos 987654321 y no 45*/
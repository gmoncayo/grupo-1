function MostrarEjer139() {

	var numberObject = new Number('things');
	function checkIsNaN(value){
		if(typeof value !== 'number'){
			return 'no es un numero';
		}else{
			return 'es un numero';
		}
	}
	console.log('comprobando si valor = things');
	console.log(checkIsNaN('1234')); //returns is not a number
	console.log(isNaN('things')); // returns true
	console.log(Number.isNaN(numberObject.valueOf())); //returns true
	console.log(Number.isNaN(4)); //returns false
	console.log(Number.isNaN(NaN)); //returns true


	// body...
}




/*Comprobando si un valor no es un número

Problema
Recibe datos y necesita verificar si el valor es un número.
Solución
Se pueden usar tanto el método global isNaN como el método Number.isNaN. También puede verificar el tipo de datos
utilizando el operador typeof.

Cómo funciona
Usando el método global isNaN, los parámetros se convierten en números y luego se evalúan. Si pasaras un
cadena o número al método en el objeto de número, devolverá falso. Esto hace posible pasar solo
parámetros que se pueden convertir de forma segura a NaN pero que no tienen el mismo valor. Además, puede usar el tipo de
operador y operadores estrictos de igualdad o desigualdad. Una forma de pensar en esto sería: "¿no es un número?"*/

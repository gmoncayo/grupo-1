function MostrarEjer149() {

	function swapNumbers(numb1, numb2){
		console.log('orden inicial = ' + numb1 + ',' + numb2);
		numb2 = numb2 - numb1; //es ahora -150
 		numb1 = numb1 + numb2; //es ahora 50
		numb2 = numb1 - numb2; //es ahora 200
		console.log('orden final = '  + numb1 + ',' + numb2);
	}
	swapNumbers(200,50);
	// body...
}

/*Intercambiando dos números sin una variable temporal

Problema
Necesitas invertir el orden de dos números.

Solución
Esto se incluye en la categoría de preguntas que puede obtener en una entrevista. Hacer que esto funcione solo implica
matemáticas que reasignarán el valor de cada variable. La segunda variable se reasigna dos veces.

Cómo funciona
Como podemos reasignar dinámicamente el valor de nuestras variables, solo hacemos un poco de matemática. Comenzamos inmediatamente
para reasignar los valores de las variables tan pronto como se inicie la función. Para ayudar, usaremos números redondos.
• num2 se reasigna al valor de 50-200 por lo que ahora es -150.
• num1 se reasigna al valor de 200 + (-150), por lo que ahora es 50.
• num2 se reasigna nuevamente al valor de 50 - (-150), por lo que ahora es 200.
La última línea es la que hay que tener en cuenta. Como es menos un número negativo, el resultado es el mismo que
adición.*/

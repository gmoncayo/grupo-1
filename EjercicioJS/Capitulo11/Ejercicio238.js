function MostrarEjer238() {
	var mapObj = new Map();
	mapObj.set('1st value', '1st key');
	mapObj.set('2nd value', '2nd key');
	mapObj.set('3rd value', '3rd key');
	mapObj.forEach(function(value, key){
		console.log('mapObj['+key+'] = ' + value); //devuelve mapObj[N value] = N key
	});
	// body...
}

/*¿Cómo iteras sobre un mapa usando forEach?
Problema
Desea recorrer cada tecla del mapa con forEach.
Solución
El método forEach funciona de la misma manera que para el objeto Set. Te dará la capacidad de ejecutar un
devolución de llamada una vez para cada tecla

Cómo funciona
El uso de forEach garantizará que se invoque una función en cada tecla que exista actualmente. La función puede
tome el valor actual, su clave y el mapa en su conjunto como parámetros cada vez que se llama a la función.*/
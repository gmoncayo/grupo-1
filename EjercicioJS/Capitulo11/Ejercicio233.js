function MostrarEjer233() {
	var mapObj = new Map();
	mapObj.set('myString', 'myString is a string key');
	console.log(mapObj.get('myString')); //myString es una clave de cadena
	var myObj = {};
	mapObj.set(myObj, 'Object is a used as a key');
	console.log(mapObj.get(myObj)); //El objeto se usa como clave
	// body...
}

/*¿Cuáles son las ventajas de usar un mapa sobre un objeto?
Problema
¿Cuándo es mejor usar un mapa en lugar de un objeto?
Solución
Si bien existen similitudes con los mapas y los objetos, los mapas pueden contener objetos y primitivas como claves o
valores.

Cómo funciona
Los mapas son similares a los objetos en que tienen pares de nombre / valor. Algunos de los métodos asociados con los mapas.
se asemeja al objeto Set (para obtener más información sobre conjuntos, consulte el Capítulo 10). Una instancia donde los mapas son
diferente a los objetos simples, es que las claves del mapa pueden ser de cualquier valor. Además, el tamaño de un mapa no necesita
para ser contado manualmente.*/
function MostrarEjer76(){
	console.log( void( 0 ) ); // Returna Indefinido
	var object = { one: 1, two: 2 };
	delete object.one; // Retorna True
	console.log( void( delete object.one ) ); // Retorna Indefindo
	console.log( object.two ); // Retorna el numero 2
	console.log( void( object.two ) ); // Retorna Indefindo
	function addNumbers(a, b) {
    return a + b
	}
	console.log( addNumbers(1, 2) ); // Retorna el numero 3
	console.log( void( addNumbers(1, 2) ) ); // Retorna Indefindo

}


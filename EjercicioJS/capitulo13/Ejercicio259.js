function MostrarEjer259() {
// Usando Throw ... Catch dentro y fuera de un generador
function *insideThrow(){
    while(true){
        try{
            yield 'inside try block';
        }catch(e){
            console.log('inside catch')
                }
    }
}
var it = insideThrow();
console.log(it.next());  //Object {value: "inside try block", done: false}
console.log(it.throw(new Error('this is an error'))) //catch runs and returns last yield value Object {value: "inside try block", done: false}
function *outsideThrow(){
    var value =  yield value;
    return value;
}
var it2 = outsideThrow();
console.log(it2.next());  //Object {value: undefined, done: false}
try{
    console.log(it2.next('outside try block'));  //Object {value: "outside try block", done: true}
    console.log(it2.throw(new Error('this is an error')));  //catch runs
}catch(e){
    console.log('outside catch')
}
}
function MostrarEjer100() {

console.log( String.fromCodePoint(0x61) );
console.log( String.fromCodePoint(0x61) + String.fromCodePoint(0x61) );
console.log( String.fromCodePoint(0x68, 0x65, 0x6C, 0x6C, 0x6F) );
console.log( String.fromCodePoint(119, 111, 114, 108, 100) );
console.log( String.fromCodePoint('0x61') ); // tener en cuenta que esta es una cadena de un hexadecimal
console.log( String.fromCodePoint(0xD83D, 0xDCD6) ); // Emoticon de libro abierto
console.log( String.fromCodePoint(0x10102) ); // Numero > 16 bits

}

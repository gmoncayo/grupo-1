function MostrarEjer34 () {

	var a = new Date();
	var b = 3.14;
	var c = "I'm a string";
	if (a instanceof Date) {
	    console.log("a es un Dato");
	} else {
	    console.log("a no es un Dato");
	}
	if (b instanceof Date) {
	    console.log("b es un Dato");
	} else {
	    console.log("b no es un Dato");
	}
	if (c instanceof Date) {
	    console.log("c es un Dato");
	}
	else {
	    console.log("c no es un Dato");
	}
	// body...
}

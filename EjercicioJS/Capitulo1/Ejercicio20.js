//Determinar si se inicializa una variable booleana
function MostrarEjer20() {

	var a = Boolean(true);
	var b = false;
	var c = "";
	var d = new Date();

	if ( typeof a === 'boolean' ) { 
	    console.log("a es un Booleano");
	} else {
	    console.log("a no es un Booleano");
	}
	if ( typeof b === 'boolean' ) {
	     console.log("b es un Booleano");
	} else {          
	     console.log("b no es un Booleano");
	}
	if ( typeof c === 'boolean' ) {
    console.log("c es un Booleano");
	} else {
	    console.log("c no es un Booleano");
	}
	if ( typeof d === 'boolean' ) {
	    console.log("d es un Booleano");
	} else {            
	    console.log("d no es un Booleano");
	}
	// body...
}

/*

Determinar si se inicializa una variable booleana
Problema
Debe probar una variable para determinar si es booleana u otro tipo de datos y determinar si es booleana
La variable ha sido inicializada.

Solución
Usando el operador typeof, podemos determinar el tipo de datos de una variable. Si la variable es un valor booleano,
typeof devolverá la cadena 'boolean'.

Cómo funciona
El operador typeof devuelve el valor de cadena Boolean para objetos booleanos. Para determinar si una variable
es un booleano, todo lo que tenemos que hacer es comparar estrictamente el valor de retorno del operador typeof con la cadena
'booleano'. Otros tipos de datos, como cadenas y números, devuelven diferentes cadenas que los representan.

*/

//Determinar si se inicializa una variable booleana
function MostrarEjer21(argument) {

	var a = Boolean(true);
	var b = false;
	var c = "";
	var d = new Date();

	if ( typeof a === 'boolean' ) { 
	    console.log("a es un Booleano");
	} else {
	    console.log("a no es un Booleano");
	}
	if ( typeof b === 'boolean' ) {
	     console.log("b es un Booleano");
	} else {          
	     console.log("b no es un Booleano");
	}
	if ( typeof c === 'boolean' ) {
    console.log("c es un Booleano");
	} else {
	    console.log("c no es un Booleano");
	}
	if ( typeof d === 'boolean' ) {
	    console.log("d es un Booleano");
	} else {            
	    console.log("d no es un Booleano");
	}
	// body...
}


function MostrarEjer37 () {

	var a = {};
	var b = "I'm a string";
	var c = 7;
	
	if ( typeof a === 'object' ) {
 	   console.log("a es un Objecto");
	} else {
	    console.log("a no es un Objecto");
	}
	if ( typeof b === 'object' ) {
	     console.log("b es un Objecto");
	} else {
	    console.log("b no es un Objecto");
	}
	if ( typeof c === 'object' ) {
	    console.log("c es un Objeto");
	} else {
	    console.log("c no es un Objecto");
	}
	// body...
}

/*
Determinar si una variable definida es un objeto
Problema
Debe probar una variable para determinar si es un objeto u otro tipo de datos.
Solución
Usando el operador typeof, podemos determinar el tipo de datos de una variable. Si la variable es un objeto, escribaof
devolverá la cadena "objeto".

Cómo funciona
El operador typeof devuelve la cadena "objeto" para todos los objetos derivados del objeto "objeto" incorporado.
Otros tipos de datos, como cadenas y números, devuelven una cadena que los representa.
*/
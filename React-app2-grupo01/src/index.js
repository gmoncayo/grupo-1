import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props){
    return (
        <button className={props.line ? "winsquare" : "square"} onClick={props.onClick}>
            {props.value}
        </button>
    )
}
  class Board extends React.Component {
    // Cuando se decida el ganador, resalte los tres cuadrados.
    renderSquare(i) {
      return (
      <Square 
        value={this.props.squares[i]} 
        onClick={() => this.props.onClick(i)}
        line={this.props.line.indexOf(i) === -1 ? null  :  true}
      />
      );
    }
    render() {
        let items = [];
        for (let i = 0; i<3 ;i++){
            let row = [];
            for (let j=0; j<3;j++){
                row.push(
                <span key={(i*3)+j}>
                    {this.renderSquare((i*3)+j)}
                </span>);
            }
            items.push(<div className="board-row" key={i}>{row}</div>)
        }
      return (
        <div>
            {items}
        </div>
      );
    }
  }
  
  class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          history: [{
            squares: Array(9).fill(null),
            clicked:[0,0],
          }],
          stepNumber: 0,
          xIsNext: true,
          active : -1,
          ascending : true,
        };
      }
      handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares).winner || squares[i]) {
          return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
          history: history.concat([{
            squares: squares,
            clicked:[Math.floor(i % 3), Math.floor(i / 3)],
          }]),
          stepNumber: history.length,
          xIsNext: !this.state.xIsNext,
        });
      }
      jumpTo(step) {
        this.setState({
          stepNumber: step,
          xIsNext: (step % 2) === 0,
          active: step,
        });
      }
      toggleAsc(){
          this.setState({
            ascending: !this.state.ascending
          });
      }
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const cal = calculateWinner(current.squares);
        // Muestra el elemento seleccionado actualmente en negrita en la lista de desplazamiento.
        const active = {fontWeight:900}
        const inactive = {fontWeight:100}
        // Indique la ubicación de cada movimiento en un formato específico
        const moves = history.map((step, move) => {
            const desc = move ?
              'Movimientos #' + move + " Posicion X :"  + step.clicked[0]+  " Posicion Y:" + step.clicked[1]:
              'Game start';
            return (
              <li key={move}>
                <button 
                style={move === this.state.active ? active : inactive}
                className ="curlist" 
                onClick={() => this.jumpTo(move)}>
                    {desc}
                </button>
              </li>
            );
          });
        
        if (!this.state.ascending){
            moves.sort((a,b)=> {
                return b.key - a.key ; 
            })
            
        }

        
        let status;
        if (cal.winner) {
        status = 'Winner: ' + cal.winner;
        }
        else if(cal.isDraw){
            status = 'Draw '
        } 
        else {
        status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }

      return (
        <div className="game">
          <div className="game-board">
            <Board 
                squares={current.squares}
                onClick={(i) => this.handleClick(i)}
                line = {cal.line}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
            <button onClick={() => this.toggleAsc()}>Change order</button>
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  
  ReactDOM.render(
    <Game />,
    document.getElementById('root')
  );

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return {
            winner : squares[a],
            line : lines[i],
            isDraw : false,
        }
      }
    }
    for (let i = 0; i < squares.length; i++){
        if (squares[i] === null){
            return {
                winner : null,
                line : [],
                isDraw : false,
            }
        }
    }
    return {
        winner : null,
        line : [],
        isDraw : true,
    }
  }